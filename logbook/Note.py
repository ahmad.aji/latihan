# SEBUAH CATATAN, MEMPERMUDAH PEMAHAMAN
# DAFTAR ISI :
# CHAPTER 1 - DJANGO PROJECT
# CHAPTER 2 - DJAGO MODELS DAN MIGRATION

############################################################
# CHAPTER 1 ################################################
# DJAGO PROJECT ############################################
# 
>>>VIRTUAL ENVIROMENT<<<
# virtualenv berguna untuk mimisahkan pengaturan dan package
# yang di install pada setiap Django projek.
# 
# Hal ini membuat perubahan yang dilakukan pada suatu projek
# tidak mempengaruhi projek lainnya
# 
>>>SYNTAX DASAR<<<
# : env\Scripts\activate
# : django-admin.py startproject <project-name>
# : django-admin.py startapp <app-name>
# 
>>>KONSEP DASAR<<<
# Function + Function = App
# App + App = Project
# 
>>>APP<<
# Suatu app diaggap aktif jika app tersebut didaftarkan di 
# pengaturan INSTALLED_APPS pada berkas settings.py (ada di 
# direktori yg namanya sama dengan nama project Django)
# 
>>>ROUTING<<<
# Routing diumpamakan sebagai pemetaan URL atau alamat web.
# http://localhost:8000 merupakan contoh URL
# http://localhost adalah alamat utamanya, sedangkan 8000
# adalah port yang digunakan
# 
# Django memiliki python module bernama URLconf yang isinya
# adalah sekumpulan pola yg Django akan coba cocokkan agar
# dapat menemukan tampilan (views.py) yang tepat
# 
>>>URL<<<
# Format penulisan URL pada Django ialah 
# url(reqex, view, kwargs=None, name=None)
# 	> reqex ialah pattern yang akan dicocokkan
# 	> view ialah fungsi untuk memproses request dan tampilan


#############################################################
# CHAPTER 2 #################################################
# DJAGO MODELS DAN MIGRATION ################################
# 
# 
>>>DATABASE<<<
Setiap kalian membuat aplikasi di Heroku, maka secara otomatis
Heroku akan membuatkan sebuah PostgreSQL Database 
>>>MODELS<<<
# Model pada Django adalah suatu sumber informasi terdefisini dan 
# detail mengenai data yang disimpan. Secara umum, setiap model
# dipetakan kedalam satu tabel database

>>>MIGRATION<<<
Migration adalah cara Django menerapkan perubahan yang dilakukan
pada Models (tambah, hapus, ubah kolom, dsb) ke dalam database
Perintah yg berhubungan dengan migration antara lain :
	> : makemigration
		digunakan untuk mencatat dan membuat migration/perubahan
		yg dilakukan models(berkas model.py)

	> : migrate
		digunakan untuk menerapkan migrations

Dalam setiap direktori app Django, terdapat direktori bernama
migrations. Direktori ini berisi berkas-berkas yg menyimpan
data migrations yg sudah dilakukan